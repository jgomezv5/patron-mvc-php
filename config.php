<?php

/*
 * Base PHP para un proyecto MVC usando un diseño de base de datos
 * orientada a obeto (con Mapeo Objeto Reacional | ORM).
 *  
 * @autor Julián Gómez
 */

define("URL", "http://localhost/MyShop");
define("LIBS", "libs/");
define("MODELS", "./models");
define("BS", "./businessLogic");
define("MODULE", "./views/modules/");

define("DB_TYPE", "mysql");
define("DB_HOST", "localhost");
define("DB_USER", "root");
define("DB_PASS", "");
define("DB_NAME", "my_db");

define("HASH_ALGO", "sha512");
define("HASH_KEY", "my_key");
define("HASH_SECRET", "my_secret");
define("SECRET_WORD", "so_secret");